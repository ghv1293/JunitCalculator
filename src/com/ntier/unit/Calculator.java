package com.ntier.unit;

public class Calculator {

	int number1;
	int number2;
	
	public Calculator(int number1, int number2){
		this.number1=number1;
		this.number2=number2;
	}
	
	public int add() {
		return number1+number2;
	}
	
	public double pow() {
		return Math.pow(number1, number2);
	}
	
	public int sub() {
		return number1-number2;
	}
	
	public int  multiply() {
		return number1*number2;
	}
	
	public int  divide() throws ArithmeticException {
		return number1/number2;
	}
}