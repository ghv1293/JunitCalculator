package com.ntier.util;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.ntier.unit.Calculator;

public class CalculatorTest {
	
	Calculator cal;

	@Before
	public void setUp() {
		cal = new Calculator(2,0);
		System.out.println("Before");
	}
	
	@Test
	public void testAdd() {
		int expected = 2;
		int actual = cal.add();
		assertEquals(expected, actual);
		System.out.println("Add");
		
	}
	
	@Test
	public void testPow() {
		int expected = 1;
		int actual = (int) cal.pow();
		assertEquals(expected, actual);
		System.out.println("Power");
		
	}
	
	@Test
	public void testSub() {
		int expected = 2;
		int actual = cal.sub();
		assertEquals(expected, actual);
		System.out.println("Subtraction");
		
	}
	
	@Test
	public void testMultiply() {
		int expected = 0;
		int actual = cal.multiply();
		assertEquals(expected, actual);
		System.out.println("Multiply");
		
	}
	
	@Rule
	public ExpectedException anException = ExpectedException.none();
	
	@Test
	public void testDivideByZero() {
		anException.expect(ArithmeticException.class);
		anException.expectMessage("/ by zero");
		int i =1/0;
		//cal.divide();
		//assertEquals(anException, actual);
		System.out.println("Exception method");
		
	}
	
	@After
	public void AfterTest() {
		System.out.println("after");
	}

}
